import usocket as socket
import uselect as select
import machine
import json
import dht
import utime

dht_obj = dht.DHT22(machine.Pin(2))
led_obj = machine.Pin(0, machine.Pin.OUT)
server  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
addr    = socket.getaddrinfo("0.0.0.0", 80)[0][-1]

# initial values
led_state       = None
data            = None
led_blink_int   = 200 # LED blink interval in miliseconds
hum_warn_thr    = 40  # humidity warning level in %
hum_alarm_thr   = 50  # humidity alarm level in %
update_interval = 10000  # humidity and temperature update interval in miliseconds.

def show_up():
    for i in range(3):
        led_obj.on()
        utime.sleep(0.5)
        led_obj.off()
        utime.sleep(0.5)

server.bind(addr)
server.listen(1)
show_up()

def update(data):
    if data == None:
        dht_obj.measure()
        data                = {}
        data["temperature"] = dht_obj.temperature()
        data["humidity"]    = dht_obj.humidity()
        data["update"]      = utime.ticks_ms()
    else:
        if utime.ticks_ms() >= data["update"] + update_interval:
            print("Time passed, updating")
            dht_obj.measure()
            data["temperature"] = dht_obj.temperature()
            data["humidity"]    = dht_obj.humidity()
            data["update"]      = utime.ticks_ms()
            print(data)
    
    return data

def humidity_notify(state, led_state):
    
    if state == "alarm":
        led_obj.on()
        
    if state == "norm":
        led_obj.off()
        
    if state == "warn":
        if led_state == None:
            led_obj.on()
            led_state = {}
            led_state["state"] = True
            led_state["update"] = utime.ticks_ms()
        else:
            if utime.ticks_ms() > led_state["update"] + led_blink_int:
                if led_state["state"] == True:
                    # print("Turning LED off")
                    led_obj.off()
                    led_state["state"]  = False
                    led_state["update"] = utime.ticks_ms()
                else:
                    # print("Turning LED on")
                    led_obj.on()
                    led_state["state"] = True
                    led_state["update"] = utime.ticks_ms()
    return led_state
          
def Client_handler(client_obj, data):
    client_obj.send(json.dumps(data))
    client_obj.close()

while True:
    r, w, err = select.select((server,), (), (), 1)
    if r:
        for readable in r:
            client, client_addr = server.accept()
            print("Access from: ", client_addr)
            try:
                Client_handler(client, data)
            except OSError as e:
                pass

    data = update(data)
    
    if data["humidity"] < hum_warn_thr:
        state = "norm"
    
    if (data["humidity"] >= hum_warn_thr) and (data["humidity"] < hum_alarm_thr):
        state = "warn"
    
    if data["humidity"] >= hum_alarm_thr:
        state = "alarm"
    
    led_state = humidity_notify(state, led_state)